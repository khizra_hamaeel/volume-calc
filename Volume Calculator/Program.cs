﻿/*
 Program Name: Volume Calculator
 Purpose: To calculate volume of various shapes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volume_Calculator
{
    class Program
    {

        public static double Volume(double rad)
        {
            return (4 / 3 * 3.14159 * (rad * rad * rad));
        }

        public static double Volume(double rad, double height)
        {
            return (3.14159 * (rad * rad) * height);
        }
        public static double Volume(double length, double width, double height)
        {
            return (length * width * height);
        }

        static void Main(string[] args)
        {
            double rad;
            double height;
            double length;
            double width;
            double result;
            rad = 0;
            height = 0;
            length = 0;
            width = 0;
            result = 0;
            bool keepGoing = true;

            do
            {
                Console.WriteLine("Please choose from these options.");
                Console.WriteLine();
                Console.WriteLine("1 - Volume of a Sphere");
                Console.WriteLine("2 - Volume of a Cylinder");
                Console.WriteLine("3 - Volume of a Rectangular Prism");

                string userAnswer = Console.ReadLine();
                if (userAnswer.Equals("1"))
                {
                    Console.WriteLine("Please enter the radius.");
                    rad = double.Parse(Console.ReadLine());
                    result = Volume(rad);
                    Console.WriteLine();
                    Console.WriteLine("The volume of the sphere is: " + result);
                    Console.WriteLine();

                }
                else if (userAnswer.Equals("2"))
                {

                    Console.WriteLine();
                    Console.WriteLine("Please enter the radius.");
                    rad = double.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter the height");
                    height = double.Parse(Console.ReadLine());
                    result = Volume(rad, height);
                    Console.WriteLine("The volume of the cylinder is: " + result);
                    Console.WriteLine();
                }
                else if (userAnswer.Equals("3"))
                {
                    Console.WriteLine("Please enter the length");
                    length = double.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter the width");
                    width = double.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter the height");
                    height = double.Parse(Console.ReadLine());
                    result = Volume(length, width, height);
                    Console.WriteLine("The volume of the rectangular prism is: " + result);
                    Console.WriteLine();
                }
                else
                {
                    keepGoing = false;
                }
            } while (keepGoing);

        }
    }
}
